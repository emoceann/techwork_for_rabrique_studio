from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        CREATE TABLE IF NOT EXISTS "notification" (
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "start_at" TIMESTAMPTZ NOT NULL,
    "msg" VARCHAR(2048) NOT NULL,
    "filter_by" VARCHAR(1024) NOT NULL,
    "finish_at" TIMESTAMPTZ NOT NULL
);
CREATE TABLE IF NOT EXISTS "user" (
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "phone_number" VARCHAR(16) NOT NULL,
    "carrier_code" VARCHAR(10) NOT NULL,
    "tag" VARCHAR(1024) NOT NULL,
    "timezone" VARCHAR(64) NOT NULL
);
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(100) NOT NULL,
    "content" JSONB NOT NULL
);"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        """
