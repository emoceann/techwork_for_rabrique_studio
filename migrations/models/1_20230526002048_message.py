from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        CREATE TABLE IF NOT EXISTS "message" (
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "status" VARCHAR(16) NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "notification_id" BIGINT REFERENCES "notification" ("id") ON DELETE SET NULL,
    "user_id" BIGINT REFERENCES "user" ("id") ON DELETE SET NULL
);;"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        DROP TABLE IF EXISTS "message";"""
