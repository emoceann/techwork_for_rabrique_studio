from pydantic import BaseSettings


class Settings(BaseSettings):
    POSTGRES_PASSWORD: str
    POSTGRES_USER: str
    POSTGRES_DB: str

    BROKER_URL: str

    NOTIFICATION_TOKEN: str

    class Config:
        env_file = "test.env"


def get_settings() -> Settings:
    return Settings()