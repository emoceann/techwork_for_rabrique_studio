import uvicorn
from fastapi import FastAPI
from tortoise import Tortoise
from src.db.config import TORTOISE_ORM

from src.users.api import router as users_router
from src.notifications.api import router as notifications_router

app = FastAPI()
app.include_router(users_router)
app.include_router(notifications_router)


@app.on_event("startup")
async def init_db():
    await Tortoise.init(
        config=TORTOISE_ORM
    )


@app.on_event("shutdown")
async def close_db():
    await Tortoise.close_connections()


@app.get('')
async def hello() -> str:
    return "hello"
