from celery import Celery
from src.settings import get_settings


settings = get_settings()


celery = Celery(broker=settings.BROKER_URL, result_backend=settings.BROKER_URL)
celery.autodiscover_tasks(packages=["src.notifications"])
