from fastapi import HTTPException

from src.users.dao import User
from src.users.models import UserIn, UserUpdate


async def commit_user(user: UserIn) -> User:
    return await User.create(**user.dict())


async def update_user(user_id: int, update_fields: UserUpdate) -> None:
    user = await User.get_or_none(id=user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    await user.update_from_dict(update_fields.dict(exclude_none=True)).save()


async def remove_user(user_id: int) -> None:
    user = await User.get_or_none(id=user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    await user.delete()
