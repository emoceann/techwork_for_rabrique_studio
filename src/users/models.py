from pydantic import BaseModel, Field, validator
from enum import Enum
from typing import Optional
import pytz


from src.users.utils import validate_phone_number

Timezones = Enum("Timezones", {x: x for x in pytz.country_timezones['ru']})


class UserBase(BaseModel):
    phone_number: str = Field(max_length=16)
    carrier_code: str = Field(max_length=5)
    tag: str = Field(max_length=1024)
    timezone: Timezones

    @validator("phone_number")
    def check_valid_phone(cls, value):
        return validate_phone_number(value)


class UserIn(UserBase):
    class Config:
        use_enum_values = True
        orm_mode = True


class UserOut(UserBase):
    id: int


class UserUpdate(UserBase):
    phone_number: Optional[str] = Field(max_length=16)
    carrier_code: Optional[str] = Field(max_length=5)
    tag: Optional[str] = Field(max_length=1024)
    timezone: Optional[Timezones]

    class Config:
        use_enum_values = True
        orm_mode = True
