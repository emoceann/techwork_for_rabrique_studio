from tortoise import Model, fields


class User(Model):
    id = fields.BigIntField(pk=True)
    phone_number = fields.CharField(max_length=16)
    carrier_code = fields.CharField(max_length=10)
    tag = fields.CharField(max_length=1024)
    timezone = fields.CharField(max_length=64)
