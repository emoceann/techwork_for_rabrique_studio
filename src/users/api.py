from fastapi import APIRouter
from src.users.models import UserOut, UserIn, UserUpdate
from src.users.services.db_service import commit_user, update_user, remove_user

router = APIRouter(prefix="/user")


@router.post("/add_user", response_model=UserOut)
async def add_user(user: UserIn):
    return await commit_user(user)


@router.put("/update_user/{user_id}", status_code=204)
async def update_user_fields(user_id: int, update_fields: UserUpdate):
    await update_user(user_id, update_fields)


@router.delete("/delete_user/{user_id}", status_code=204)
async def delete_user(user_id: int):
    return await remove_user(user_id)
