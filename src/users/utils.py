import re


def validate_phone_number(value):
    regex = r"^(\+)[7][0-9\-\(\)\.]{9,15}$"
    if not re.search(regex, value):
        raise ValueError("Phone number is not valid")
    return value
