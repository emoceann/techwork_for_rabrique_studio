from src.settings import get_settings

settings = get_settings()


TORTOISE_ORM = {
    "connections": {
        "default": f"asyncpg://{settings.POSTGRES_USER}:{settings.POSTGRES_PASSWORD}@postgres/{settings.POSTGRES_DB}"
    },
    "apps": {
        "models": {
            "models": ["src.notifications.dao", "src.users.dao", "aerich.models"],
            "default_connection": "default",
        },
    },
}
