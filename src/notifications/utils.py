from src.notifications.models import NotificationOut
from src.users.dao import User
from src.settings import get_settings
from datetime import datetime

import httpx
import pytz


async def check_datetime(start_at: datetime, finish_at: datetime):
    now = datetime.now(tz=pytz.UTC)
    return start_at < now < finish_at


async def request_to_api(user: User, notification: NotificationOut, settings=get_settings()):
    data = {
        "id": notification.id,
        "phone": user.phone_number,
        "text": notification.msg
    }
    headers = {"Authorization": f"Bearer {settings.NOTIFICATION_TOKEN}"}
    async with httpx.AsyncClient() as client:
        response = await client.post(f"https://probe.fbrq.cloud/v1/send/{notification.id}", json=data, headers=headers)
        return response
