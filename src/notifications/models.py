from typing import Optional

import pytz
from pydantic import BaseModel, Field, validator
from datetime import datetime


class NotificationBase(BaseModel):
    filter_by: str = Field(max_length=1024)
    msg: str = Field(max_length=2048)


class NotificationIn(NotificationBase):
    start_at: datetime
    finish_at: datetime

    @validator("finish_at")
    def check_start_at(cls, value):
        if value < datetime.now(tz=pytz.UTC):
            raise ValueError("Invalid datetime")
        return value


class NotificationOut(NotificationIn):
    id: int

    class Config:
        orm_mode = True


class NotificationStats(NotificationBase):
    id: int
    ok_status: int
    other_status: int
    start_at: datetime
    finish_at: datetime

    class Config:
        orm_mode = True


class NotificationUpdate(NotificationIn):
    start_at: Optional[datetime]
    finish_at: Optional[datetime]
    filter_by: Optional[str] = Field(max_length=1024)
    msg: Optional[str] = Field(max_length=2048)

