from src.main_celery.app import celery
from src.notifications.models import NotificationOut
from src.notifications.services.tasks_service import notifier
import asyncio


@celery.task
def notify_task(notification: dict) -> None:
    notification = NotificationOut.parse_obj(notification)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(notifier(notification=notification))

