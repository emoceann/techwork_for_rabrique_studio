from fastapi import APIRouter, Depends
from src.notifications.services.notify_service import NotificationService
from src.notifications.services.db_service import get_all_notifications_with_message_count, remove_notification, get_detail_stat_one_notification, update_notification
from src.notifications.models import NotificationIn, NotificationOut, NotificationStats, NotificationUpdate

router = APIRouter(prefix="/notifications")


@router.post("/create", response_model=NotificationOut)
async def create_notification(data: NotificationIn, service: NotificationService = Depends()):
    return await service.process_notification(data)


@router.put("/update/{notification_id}", status_code=204)
async def update_notifcation(notification_id: int, update_fields: NotificationUpdate):
    return await update_notification(notification_id, update_fields)


@router.get("/all_stats", response_model=list[NotificationStats])
async def get_all_stats():
    return await get_all_notifications_with_message_count()


@router.delete("/delete/{notification_id}", status_code=204)
async def delete_notification(notification_id: int):
    await remove_notification(notification_id)


@router.get("/get_info/{notification_id}", response_model=NotificationStats)
async def get_notification_info(notification_id: int):
    return await get_detail_stat_one_notification(notification_id)