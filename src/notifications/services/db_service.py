from fastapi import HTTPException
from tortoise.expressions import Q

from src.notifications.dao import Notification, Message
from src.notifications.models import NotificationIn, NotificationStats, NotificationUpdate
from tortoise.functions import Count


async def create_notification(notification: NotificationIn) -> Notification:
    return await Notification.create(**notification.dict())


async def get_stats(notification: Notification):
    ok_status_stat = await Message.filter(
        notification_id=notification.id
    ).annotate(ok_status=Count("status", _filter=Q(status="OK"))).values("ok_status")
    other_status_stat = await Message.filter(
        notification_id=notification.id
    ).exclude(status="OK").count()
    return ok_status_stat, other_status_stat


async def get_all_notifications_with_message_count():  # -> Notification:
    result = []
    for notification in await Notification.all():
        ok_status_stat, other_status_stat = await get_stats(notification)
        result.append(
            NotificationStats(
                id=notification.id, filter_by=notification.filter_by, start_at=notification.start_at,
                finish_at=notification.finish_at, msg=notification.msg, ok_status=ok_status_stat[0]["ok_status"],
                other_status=other_status_stat
            )
        )
    return result


async def get_detail_stat_one_notification(notification_id: int) -> NotificationStats:
    notification = await Notification.get_or_none(id=notification_id)
    if not notification:
        raise HTTPException(status_code=404, detail="Notification not found")
    ok_status_stat, other_status_stat = await get_stats(notification)
    return NotificationStats(
        id=notification.id, filter_by=notification.filter_by, start_at=notification.start_at,
        finish_at=notification.finish_at, msg=notification.msg, ok_status=ok_status_stat[0]["ok_status"],
        other_status=other_status_stat
    )


async def remove_notification(notification_id: int) -> None:
    notification = await Notification.get_or_none(id=notification_id)
    if not notification:
        raise HTTPException(status_code=404, detail="Notification not found")
    await notification.delete()


async def update_notification(notification_id: int, update_fields: NotificationUpdate) -> None:
    notification = await Notification.get_or_none(id=notification_id)
    if not notification:
        raise HTTPException(status_code=404, detail="Notification not found")
    await notification.update_from_dict(update_fields.dict(exclude_none=True)).save()
