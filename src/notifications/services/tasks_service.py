from src.notifications.models import NotificationOut
from src.notifications.utils import check_datetime, request_to_api
from src.users.dao import User
from src.notifications.dao import Message
from tortoise.expressions import Q
from tortoise import Tortoise
from src.db.config import TORTOISE_ORM


async def notifier(notification: NotificationOut) -> None:
    await Tortoise.init(config=TORTOISE_ORM)
    users = await User.filter(Q(Q(carrier_code=notification.filter_by), Q(tag=notification.filter_by), join_type="OR"))
    for user in users:
        if not await check_datetime(start_at=notification.start_at, finish_at=notification.finish_at):
            return
        response = (await request_to_api(user, notification)).json()
        await Message.create(status=response["message"], notification_id=notification.id, user_id=user.id)
    await Tortoise.close_connections()
