from src.notifications.models import NotificationIn, NotificationOut
from src.notifications.services.db_service import create_notification
from src.notifications.utils import check_datetime
from src.notifications.tasks import notify_task


class NotificationService:

    async def process_notification(self, notification: NotificationIn) -> NotificationOut:
        notification = await create_notification(notification)
        notification = NotificationOut.from_orm(notification)
        if await check_datetime(start_at=notification.start_at, finish_at=notification.finish_at):
            await self.notify_immediately(notification)
        else:
            await self.notify_after(notification)
        return notification

    async def notify_immediately(self, notification: NotificationOut):
        notify_task.apply_async(kwargs={"notification": notification.dict()})

    async def notify_after(self, notification: NotificationOut):
        notify_task.apply_async(kwargs={"notification": notification.dict()}, eta=notification.start_at)
