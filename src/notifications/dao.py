from tortoise import Model, fields


class Notification(Model):
    id = fields.BigIntField(pk=True)
    start_at = fields.DatetimeField()
    msg = fields.CharField(max_length=2048)
    filter_by = fields.CharField(max_length=1024)
    finish_at = fields.DatetimeField()


class Message(Model):
    id = fields.BigIntField(pk=True)
    status = fields.CharField(max_length=16)
    created_at = fields.DatetimeField(auto_now=True)
    notification = fields.ForeignKeyField("models.Notification", on_delete=fields.SET_NULL, null=True)
    user = fields.ForeignKeyField("models.User", on_delete=fields.SET_NULL, null=True)
